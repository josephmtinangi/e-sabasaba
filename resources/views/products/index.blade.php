@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2>Products</h2>

                        @if($products->count() > 0)

                            @foreach($products->chunk(3) as $productsSet)

                                <div class="row">

                                    @foreach($productsSet as $product)

                                        <div class="col-sm-4">
                                            <div class="thumbnail">
                                                <img src="http://lorempixel.com/400/200/nature/1" class="img-responsive" alt="">
                                                <div class="caption">
                                                    <h3><a href="{{ url('products/' . $product->id) }}">{{ $product->name }}</a></h3>
                                                    <p>
                                                        {{ $product->price }}
                                                    </p>
                                                    <p>
                                                        {{ $product->supply_ability }}
                                                    </p>
                                                    <p>
                                                        {{ $product->description }}
                                                    </p>
                                                    <p>
                                                        {{ $product->user->email }}
                                                    </p>
                                                    <p>
                                                        <a href="#" class="btn btn-primary">More</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                </div>

                            @endforeach

                        @else

                            No product

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
