<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'description',
        'price',
        'supply_ability',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
